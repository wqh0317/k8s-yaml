helm介绍：可以将它类比为Centos系统中的yum命令，通过helm可以快速安装一些软件（但有些软件的版本更新并不是很及时，需要手动更新yaml中的镜像）



```

wget https://get.helm.sh/helm-v3.3.4-linux-amd64.tar.gz
//解压二进制文件到bin

//添加helm源
helm repo add stableb https:/kubernetes-charts.storage.googleapis.com  
helm repo add aliyunb https:/kubernetes.oss-cn-hangzhou.aliyuncs.com/charts 
helm repo add  apphub https://apphub.aliyuncs.com/ 

//使用示例
helm pull aliyun/gitlab-ce
//可以看到版本较老
tar -xzvf gitlab-ce-0.2.1.tgz 
➜  cd gitlab-ce 
//目录中有安装gitlab的所有相关文件，这种方式在熟悉了k8s基本资源对象后就可以通过修改模版快速部署应用
➜  gitlab-ce ls
charts  Chart.yaml  README.md  requirements.lock  requirements.yaml  templates  values.yaml
```



