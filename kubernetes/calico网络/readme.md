```
//请根据版本更新calico版本
wget https://projectcalico.docs.tigera.io/v3.22/manifests/calico.yaml --no-check-certificate 
```

### calico坑点

1. 请确保局域网段无冲突（确认方式：ifconfig中，与下面设置的网段无冲突，默认为192.168.0.0/16）

   ```
   # The default IPv4 pool to create on startup if none exists. Pod IPs will be
   # chosen from this range. Changing this value after installation will have
   # no effect. This should fall within `--cluster-cidr`.
   # - name: CALICO_IPV4POOL_CIDR
   #   value: "192.168.0.0/16"
   # Disable file logging so `kubectl logs` works.
   ```

2. 确保dns服务器的网络连接正确，如无法匹配将导致节点无法正常Ready

   ```
   #显示当前网络连接
   #nmcli connection show
   

   如大楼服务器为ens192 温州服务器为eth0
   找到
    - name: CLUSTER_TYPE
    value: "k8s,bgp"
    加入下列属性属性设置为这样
    - name: IP_AUTODETECTION_METHOD
      value: "interface=eth.*|en.*|em.*|bo.*"
   ```

   

