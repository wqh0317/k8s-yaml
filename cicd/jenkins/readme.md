参考：https://www.modb.pro/db/116612



### 部署jenkins及相关插件

###### 部署jenkins

`kubectl create ns cicd`

`kubectl create -f .`



###### 获取日志中jenkins密码，登陆jenkins

`kubectl logs jenkins-0 -ncicd`



###### 配置jenkins加速

```
cd /data/kubernetes/jenkins/updates
sed -i 's#https://updates.jenkins.io/download#https://mirrors.tuna.tsinghua.edu.cn/jenkins#g;s#https://www.google.com#https://www.baidu.com#g' default.json
```



###### 安装插件

kubernetes plugin、blue ocean



###### 配置K8s插件，连接集群

```
1. 复制cfssl工具
cp k8s-utils/* /usr/local/bin

2.准备证书签名请求
vim admin-csr.json
{
  "CN": "admin",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "HangZhou",
      "L": "XS",
      "O": "system:masters",
      "OU": "System"
    }
  ]
}

3.创建证书
cfssl gencert -ca=/etc/kubernetes/pki/ca.crt -ca-key=/etc/kubernetes/pki/ca.key --profile=kubernetes admin-csr.json | cfssljson -bare admin
//最终生成以下3个文件：admin.csr、admin-key.pem、admin.pem

4. 通过openssl来转换成pkc格式：
openssl pkcs12 -export -out ./jenkins-admin.pfx -inkey ./admin-key.pem -in ./admin.pem -passout pass:secret

5.配置jenkins认证
//http://10.140.21.16:30006/configureClouds/
在集群配置的kubernetes配置中，点击凭据-添加：类型选择Certificate，Upload生成的jenkins-admin.pfk文件，测试集群连接
```



###### 根据需要配置jenkins slave的PodTemplate

![](images/cluster.jpg)

注意，pod的Container Template name 必须为jnlp，否则jenkins在运行slave时会再启动一个agent容器，导致部署失败



###### 配置pipeline实现持续部署

![image-20220228155645454](images/pipeline.jpg)

参考https://www.modb.pro/db/116612中，配置git地址和jenkinsfile在文件中的名称，学习jenkinsfile语法构建pipeline
